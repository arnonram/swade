export default interface ISKillOptions {
  event: any;
  additionalMods?: (string | number)[];
}
